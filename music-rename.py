import argparse
import mutagen
import os
import sys
import string
import re

def eprint(s):
	sys.stderr.write(s + '\n')

class TagFormatter(string.Formatter):
	def __init__(self, m):
		super(TagFormatter, self).__init__()
		self._m = m

	class _WrapperFormat:
		def __init__(self, s, pre, post = ''):
			self.s = s
			self.pre = pre
			self.post = post
		def __str__(self):
			return self.s
		def __repr__(self):
			return repr(dict(s = self.s, pre = self.pre, post = self.post))

	def vformat(self, format_string, args, kwargs):
		used_args = set()
		ret = ''.join(self._vformat_gen(format_string, args, kwargs, used_args))
		self.check_unused_args(used_args, args, kwargs)
		return ret
	def _vformat_gen(self, format_string, args, kwargs, used_args):
		auto_arg_index = 0
		for txt, field, f_spec, conv in self.parse(format_string):
			yield txt
			if field is not None:
				if field == '':
					auto_arg_index += 1
				v, k = self.get_field(field, args, kwargs)
				used_args.add(k)
				v = self.convert_field(v, conv)
				yield self.format_field(v, f_spec)

	def parse(self, fmt):
		if hasattr(fmt, 'pre'): # If we have a Wrapper Format already
			return [(fmt, None, None, None)]
		else:
			return list(self._parse(fmt))

	def _parse(self, fmt):
		length = len(fmt)
		block = []
		pos = 0
		pos_last_end = 0
		f_spec = None
		while pos < length:
			if fmt[pos] == '{':
				if not block and (pos > pos_last_end):
					yield (fmt[pos_last_end:pos], None, None, None)
				block.append(pos)
			elif fmt[pos] == '}':
				if block:
					pos_start = block.pop()
				else:
					raise ValueError('Unmatched closing brace ("}}") at position {} of format string: {}'.format(pos, fmt))
				if block:
					for (txt, field, f_spec, conv) in super().parse(fmt[pos_start:pos + 1]):
						pos_static_start = block.pop() + 1
						f_spec = self._WrapperFormat(f_spec, fmt[pos_static_start:pos_start])
						block.append(pos)
						yield (txt, field, f_spec, conv)
				else:
					if f_spec is None:
						# If no child bracketed expression, just parse the entire bracketed expression
						for t in super().parse(fmt[pos_start:pos + 1]):
							yield t
					else:
						# If surrounding other bracketed expression, append remainder to prior format
						f_spec.post = fmt[pos_start + 1:pos]
					f_spec = None
				pos_last_end = pos + 1
			pos += 1

		if block:
			raise ValueError('Unmatched opening brace ("{{") at position {} of format string: {}'.format(block[-1], fmt))

		if pos > pos_last_end: # Any remainder
			yield (fmt[pos_last_end:pos], None, None, None)

	def parse_orig(self, fmt):
		while pos < length:
			pos_new = fmt.find('{', pos)
			pos_close = fmt.find('}', pos)
			if pos_new < 0:
				s_post = fmt[pos_close + 1:]
				pos = pos_close + 1
			else:
				s_pre = fmt[pos + 1:pos_new]
				block.append(pos_new)

	def get_value(self, key, *args):
		try:
			return super().get_value(key, *args)
		except KeyError:
			return self._m.get(key, [''])[0]
	def format_field(self, val, fmt):
		s_fmt = fmt.s if hasattr(fmt, 'pre') else fmt
		try:
			r =  super().format_field(val, s_fmt)
		except ValueError:
			r = '' if val == '' else super().format_field(int(val), s_fmt)
		return (fmt.pre + r + fmt.post) if (r and (fmt != s_fmt)) else r

def _rename(items, force):
	for source, dest in items:
		if os.path.normpath(source) == os.path.normpath(dest):
			continue
		try:
			d, f = os.path.split(dest)
			if d:
				os.makedirs(d, exist_ok=True)
			if not force and os.path.exists(dest):
				raise OSError(-1, 'Prevented attempt to overwrite existing file')
			os.rename(source, dest)
		except OSError as ex:
			eprint('Failed to rename {}'.format(source))
			eprint('{}: {}'.format(ex.errno, ex.strerror))

def _gather_paths(paths, pattern):
	generate_name = lambda m, f: TagFormatter(m).format(pattern) + os.path.splitext(f)[-1]
	rx = re.compile('[/]+')
	for path in paths:
		pathnew = rx.sub('/', _get_new_name(path, generate_name))
		if pathnew.startswith('/') and not pattern.startswith('/'):
			pathnew = pathnew[1:] # Don't cause rooted path if a Tag is missing
		yield (path, pathnew)

class InvalidResponseError(ValueError):
	pass

def _display_rename(paths, pattern):
	items = list(_gather_paths(paths, pattern))
	for source, dest in items:
		print('{} {}'.format(source, dest))
	try:
		next(s for s, d in items if s != d)
	except StopIteration:
		eprint('No file renames would occur - all files have names that match pattern already')
		return []
	response = input('Proceed with rename operation? (y/N) ')
	response_u = response.upper()
	if 'NO'.startswith(response_u):
		return []
	elif 'YES'.startswith(response_u):
		return items
	else:
		raise InvalidResponseError(response)

def _get_new_name(path, generate_name):
	m = mutagen.File(path, easy=True)
	base, name = os.path.split(path)
	path_new = generate_name(m, path)
	base_new, name_new = os.path.split(path_new)
	return path_new if (len(base_new) > 0) else os.path.join(base, name_new)

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument(dest='rename_pattern', metavar='PATTERN'
		, help="""Rename files according to PATTERN from file Tags
			(similar to Python's format strings, except no support for
			nested format expressions in the extra format specifiers, and nested braces
			indicate to output entire content only if inner content has a value. E.g.,
			"{artist}{ - {album}}/{{tracknumber}. }{title}")""")
	parser.add_argument(dest='source', metavar='FILE', nargs='*', help="""Rename FILE(s) (or files if FILE is a directory; default to current directory if no argument)""")
	parser.add_argument('-r', '--recursive', dest='recursive', action='store_true', help="""Recurse subdirectories if FILE is a directory (no effect if FILE is not a directory)""")
	parser.add_argument('-i', '--interactive', dest='interactive', action='store_true', help="""Show new filenames and confirm before performing filesystem rename""")
	parser.add_argument('-f', '--force', dest='force', action='store_true', help="""Force renaming of file, allowing overwrite without prompting""")

	args = parser.parse_args()

	name_input = lambda root, name: os.path.join(root, name)
	if not args.source:
		args.source = [os.curdir]
		name_input = lambda root, name: os.path.relpath(os.path.join(root, name)) # Don't prepend './' to name

	for source in args.source:
		if os.path.isdir(source):
			if args.recursive:
				files = (name_input(root, name)
					for (root, dirs, names) in os.walk(source)
					for name in names)
			else:
				files = (os.path.join(p, name) for name in os.listdir(source))
		else:
			files = [source]

		if args.interactive:
			try:
				items = _display_rename(files, args.rename_pattern)
			except InvalidResponseError as ex:
				eprint('Unknown response: ' + str(ex))
				sys.exit(1)
		else:
			items = _gather_paths(files, args.rename_pattern)
		_rename(items, args.force)

